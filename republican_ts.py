#!/usr/bin/python3
import os
import time
import subprocess
import argparse
import pickle
import getpass
import hashlib
import readline
import base64

# Note pour le passage du linux à ubuntu for windows
# Sur linux, la commande est openssl enc aes-256-cbc -salt -k password -iter 100
# Sur ubuntu for windows, la commande est openssl enc -aes-256-cbc -salt -k password

SECURE_PATH = "vault.pk"
# If not screen, then tmux
SCREEN = True

ENCRYPT_QUERY = "echo -n '{}' | openssl enc -aes-256-cbc -salt -k {} -base64 -iter 100"
DECRYPT_QUERY = "echo -n '{}' | openssl enc -d -aes-256-cbc -salt -k {} -base64 -iter 100"

def check(master_pass):
    """
    Init the pickle, create the key and check the password
    """
    h = hashlib.sha512()
    h.update(master_pass.encode())
    # Erase password
    master_pass = "0"*len(master_pass)
    del master_pass
    key = h.hexdigest()

    if not os.path.exists(SECURE_PATH):
        print("INFO: Create a new vault at " + SECURE_PATH)
        secure_dico = dict()
        encrypted = encrypt(key, "ok")
        secure_dico["check"] = encrypted
        write_secure(secure_dico)
    else:
        secure_dico = read_secure()
        encrypted = secure_dico["check"]
        message = decrypt(key, encrypted)

    return key

def encrypt(key, message):
    """
    Encrypt a message
    """
    try:
        message = base64.urlsafe_b64encode(message.encode())
        encrypted = subprocess.check_output(ENCRYPT_QUERY.format(message.decode(), key), shell=True)
    except:
        print("ERROR: Fail to encrypt")
        exit(1)

    return encrypted.decode()

def decrypt(key, encrypted):
    """
    Decrypt a message
    """
    try:
        message = subprocess.check_output(DECRYPT_QUERY.format(encrypted, key), shell=True)
        message = base64.urlsafe_b64decode(message)
    except:
        print("ERROR: Fail to decrypt")
        exit(1)

    return message.decode()

def e_input(prompt, prefill=""):
    """
    Editable input using readline
    """
    readline.set_startup_hook(lambda: readline.insert_text(prefill))
    try:
        return input(prompt)
    finally:
        readline.set_startup_hook()

def run_entry(key, entry):
    """
    Run an entry
    """
    secure_dico = read_secure()
    if not entry in secure_dico.keys():
        print("ERROR: Entry not found")
        return

    # Print description
    print("INFO: " + decrypt(key, secure_dico[entry][0]))
    try:
        if SCREEN == True:
            windows = subprocess.check_output("screen -Q windows", shell=True).decode().split()
            window = entry
            incr = 1
            while window in windows:
                incr += 1
                window = entry + "-" + str(incr)
            retcode = subprocess.call("screen -X screen -t " + window, shell=True)
            for encrypted in secure_dico[entry][1:]:
                command = decrypt(key, encrypted)
                if command[:4] == "wait":
                    wait = int(command.split(" ")[1])
                    time.sleep(wait)
                elif command == "enter":
                    retcode = subprocess.call("screen -p {} -X stuff '{}'".format(window, "\n"), shell=True)
                else:
                    command = command.replace("'", "'\\''")
                    retcode = subprocess.call("screen -p {} -X stuff '{}'".format(window, command), shell=True)
                command = "0"*len(command)
                del command
                time.sleep(1)
        else:
            windows = subprocess.check_output("tmux list-window -F '#{window_name}'", shell=True).decode().split("\n")
            window = entry
            incr = 1
            while window in windows:
                incr += 1
                window = entry + "-" + str(incr)
            retcode = subprocess.call("tmux new-window", shell=True)
            retcode = subprocess.call("tmux rename-window " + window, shell=True)

            for encrypted in secure_dico[entry][1:]:
                command = decrypt(key, encrypted)
                if command[:4] == "wait":
                    wait = int(command.split(" ")[1])
                    time.sleep(wait)
                else:
                    command = command.replace("'", "'\\''")
                    retcode = subprocess.call("tmux send-keys -t {} -- '{}'".format(window, command), shell=True)
                command = "0"*len(command)
                del command
                time.sleep(1)
    except:
        print("ERROR: Screen/tmux error")
        return

def test_entry(key, entry):
    """
    Test an entry
    """
    secure_dico = read_secure()
    if not entry in secure_dico.keys():
        print("ERROR: Entry not found")
        return

    # Print description
    print("INFO: " + decrypt(key, secure_dico[entry][0]))
    try:
        if SCREEN == True:
            retcode = subprocess.call("screen -X split -v", shell=True)
            retcode = subprocess.call("screen -X focus next; screen -X screen -t {}; screen -X focus next".format(entry), shell=True)
            for encrypted in secure_dico[entry][1:]:
                command = decrypt(key, encrypted)
                if command[:4] == "wait":
                    wait = int(command.split(" ")[1])
                    time.sleep(wait)
                elif command == "enter":
                    retcode = subprocess.call("screen -p {} -X stuff '{}'".format(entry, "\n"), shell=True)
                else:
                    command = command.replace("'", "'\\''")
                    retcode = subprocess.call("screen -p {} -X stuff '{}'".format(entry, command), shell=True)
                command = "0"*len(command)
                del command
                rep = input("ok? (o/n) ")
                if rep != "o":
                    print("INFO: Stopping test")
                    return
        else:
            retcode = subprocess.call("tmux split-window -hd", shell=True)

            for encrypted in secure_dico[entry][1:]:
                command = decrypt(key, encrypted)
                if command[:4] == "wait":
                    wait = int(command.split(" ")[1])
                    time.sleep(wait)
                else:
                    command = command.replace("'", "'\\''")
                    retcode = subprocess.call("tmux send-keys -t + '{}'".format(command), shell=True)
                command = "0"*len(command)
                del command
                rep = input("ok? (o/n) ")
                if rep != "o":
                    print("INFO: Stopping test")
                    return

    except:
        print("ERROR: Screen/tmux error")
        return

def write_secure(secure_dico):
    """
    Store the dict in the pickle
    """
    with open(SECURE_PATH, "wb") as fichier:
        pickle.dump(secure_dico, fichier)

def read_secure():
    """
    Read the dict from the pickle
    """
    with open(SECURE_PATH, "rb") as fichier:
        secure_dico = pickle.load(fichier)
    return secure_dico

def create_entry(key, entry):
    """
    Create one entry
    """
    secure_dico = read_secure()
    secure_dico[entry] = list()
    write_secure(secure_dico)

    commands = list()
    command = input("Description: ")
    commands.append(command)

    print("INFO: What commands (bash, enter, wait N)...")
    command = input("> ")
    while command != "":
        commands.append(command)
        command = "0"*len(command)
        command = input("> ")
    for command in commands:
        secure_dico = read_secure()
        encrypted = encrypt(key, command)
        secure_dico[entry].append(encrypted)
        write_secure(secure_dico)
    for i in range(len(commands)):
        commands[i] = "0"*len(commands[i])

def edit_entry(key, entry):
    """
    Edit one entry
    """
    print("INFO: Preview of " + entry)
    print("=====")
    show_entry(key, entry)
    print("=====")
    print("INFO: Edition of " + entry)
    secure_dico = read_secure()
    try:
        length = len(secure_dico[entry])
    except:
        print("ERR: No entry " + entry)
        return
    i = 0
    while i < length:
        secure_dico = read_secure()
        command = decrypt(key, secure_dico[entry][i])
        if i == 0:
            command = e_input("Description: ", command)
        else:
            command = e_input("> ", command)
        if command == "" and i != 0:
            secure_dico[entry].pop(i)
            i -= 1
        elif command[-4:] == " ADD":
            encrypted = encrypt(key, command[:-4])
            secure_dico[entry][i] = encrypted
            encrypted = encrypt(key, "")
            secure_dico[entry].insert(i+1, encrypted)
        else:
            encrypted = encrypt(key, command)
            secure_dico[entry][i] = encrypted
        write_secure(secure_dico)
        i += 1
        secure_dico = read_secure()
        length = len(secure_dico[entry])
    
def show_entry(key, entry):
    """
    Show one entry
    """
    secure_dico = read_secure()
    try:
        print("Description: " + decrypt(key, secure_dico[entry][0]))
    except:
        print("ERROR: No entry")
        return
    for message in secure_dico[entry][1:]:
        command = decrypt(key, message)
        print("> " + command)
    
def duplicate_entry(key, entry, new):
    """
    Duplicate entry to a new one
    """
    secure_dico = read_secure()
    secure_dico[new] = list()
    write_secure(secure_dico)

    commands = list()
    secure_dico = read_secure()
    for encrypted in secure_dico[entry]:
        commands.append(encrypted)

    for command in commands:
        secure_dico = read_secure()
        secure_dico[new].append(command)
        write_secure(secure_dico)
    for i in range(len(commands)):
        commands[i] = "0"*len(commands[i])

def list_entries(key, string):
    """
    List entries
    """
    liste_tmp = list()
    secure_dico = read_secure()
    for k in secure_dico.keys():
        if k != "check":
            cond = True
            for elt in string.split():
                cond = cond and (elt.lower() in k.lower() or elt.lower() in decrypt(key, secure_dico[k][0]).lower())
            if cond:
                liste_tmp.append((k, decrypt(key, secure_dico[k][0])))
    #liste_tmp.extend([(k, decrypt(key, secure_dico[k][0])) for k in secure_dico.keys() if k != "check" and (elt.lower() in k.lower() or elt.lower() in decrypt(key, secure_dico[k][0]).lower())])
    liste_tmp.sort(key=lambda tup: tup[1])
    print("\n".join([ "* {:<15}: {}".format(elt[0], elt[1]) for elt in liste_tmp]))
    
def run():
    """
    Run mode
    """
    entry = input("What to do> ")
    while entry != "QUIT":
        if entry == "HELP":
            print("HELP         -- This help")
            print("entry        -- Run an entry")
            print("LIST [word]  -- List entries")
            print("ADD entry    -- Add an entry")
            print("EDIT entry   -- Edit an entry")
            print("SHOW entry   -- Show an entry")
            print("TEST entry   -- Test an entry")
            print("DEL entry    -- Delete an entry")
            print("DUP old new  -- Duplicate old entry to new one")
            print("QUIT         -- Exit the program")

        elif entry[:5] == "LIST ":
            print("INFO: List entries...")
            list_entries(key, entry[5:])

        elif entry == "LIST":
            print("INFO: List entries...")
            secure_dico = read_secure()
            liste_tmp = [(k, decrypt(key, secure_dico[k][0])) for k in secure_dico.keys() if k != "check"]
            liste_tmp.sort(key=lambda tup: tup[1])
            print("\n".join([ "* {:<15}: {}".format(elt[0], elt[1]) for elt in liste_tmp]))

        elif entry[:4] == "ADD " and entry[4:] != "check":
            print("INFO: Create entry " + entry[4:])
            create_entry(key, entry[4:])

        elif entry[:5] == "EDIT " and entry[5:] != "check":
            print("INFO: Edit entry " + entry[5:])
            edit_entry(key, entry[5:])

        elif entry[:5] == "TEST " and entry[5:] != "check":
            print("INFO: Test entry " + entry[5:])
            test_entry(key, entry[5:])

        elif entry[:5] == "SHOW " and entry[5:] != "check":
            print("INFO: show entry " + entry[5:])
            show_entry(key, entry[5:])

        elif entry[:4] == "DEL " and entry[4:] != "check":
            print("INFO: Delete entry " + entry[4:])
            secure_dico = read_secure()
            secure_dico.pop(entry[4:], None)
            write_secure(secure_dico)

        elif entry[:4] == "DUP " and entry[4:].count(" ") == 1 and entry[4:].split(" ")[0] != "check" and entry[4:].split(" ")[1] != "check":
            print("INFO: Duplicate " + entry[4:].split(" ")[0] + " to " + entry[4:].split(" ")[1])
            duplicate_entry(key, entry[4:].split(" ")[0], entry[4:].split(" ")[1])

        elif entry != "check" and entry != "":
            print("INFO: Run entry " + entry)
            run_entry(key, entry)

        entry = input("What to do> ")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", help="Init vault", action="store_true")
    parser.add_argument("-a", help="Add entry")
    parser.add_argument("-s", help="Show entry")
    parser.add_argument("-e", help="Edit entry")
    parser.add_argument("-t", help="Test entry")
    parser.add_argument("-d", help="Delete entry")
    parser.add_argument("-r", help="Run entry")
    parser.add_argument("-l", help="List entries", action="store_true")
    args = parser.parse_args()
    print("""=================
\\__o< ~ coin coin
=================""")

    if args.i:
        print("INFO: Delete vault. The next run will create a new one.")
        os.system("rm "+SECURE_PATH)
        exit(0)

    master_pass = getpass.getpass("Master password: ")
    key = check(master_pass)
    master_pass = "0"*len(master_pass)
    del master_pass

    # Ok, "check" is a special key used by the program, do not touch it
    if args.a and args.a != "check":
        print("INFO: Create entry " + args.a)
        create_entry(key, args.a)

    elif args.e and args.e != "check":
        print("INFO: Edit entry " + args.e)
        edit_entry(key, args.e)

    elif args.s and args.s != "check":
        print("INFO: show entry " + args.s)
        show_entry(key, args.s)

    elif args.t and args.t != "check":
        print("INFO: Test entry " + args.t)
        test_entry(key, args.t)

    elif args.d and args.d != "check":
        print("INFO: Delete entry " + args.d)
        secure_dico = read_secure()
        secure_dico.pop(args.d, None)
        write_secure(secure_dico)
        print("INFO: Done")

    elif args.r and args.r != "check":
        print("INFO: Run entry " + args.r)
        run_entry(key, args.r)

    elif args.l:
        print("INFO: List entries...")
        secure_dico = read_secure()
        liste_tmp = [(k, decrypt(key, secure_dico[k][0])) for k in secure_dico.keys() if k != "check"]
        liste_tmp.sort(key=lambda tup: tup[1])
        print("\n".join([ "* {:<15}: {}".format(elt[0], elt[1]) for elt in liste_tmp]))

    else:
        try:
            run()
        except KeyboardInterrupt as e:
            print("\nINFO: Program stopped by user.")
        except EOFError as e:
            print("\nINFO: Program stopped by user.")

    print("INFO: Bye!")
