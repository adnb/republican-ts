# Republican TS

Republican TS is a key sequence task manager for unix terminal. It allows to:

* record commands tasks sequence
* secure them in an encrypted vault
* play them to automate some commands, like ssh connection, sudo and su

# Usage

Republican TS must be run inside a screen session or a tmux session. The choice between screen and tmux is inside the program, with SCREEN global variable.

```
usage: ./republican_ts.py [-h] [-i] [-a A] [-s S] [-e E] [-t T] [-d D] [-r R]
                        [-l]

optional arguments:
  -h, --help  show this help message and exit
  -i          Init vault
  -a A        Add entry
  -s S        Show entry
  -e E        Edit entry
  -t T        Test entry
  -d D        Delete entry
  -r R        Run entry
  -l          List entries
```

With no arguments, Republican TS loops on the run mode.

```
$ ./republican_ts.py 
=================
\__o< ~ coin coin
=================
Master password: 
What to do> HELP
HELP         -- This help
entry        -- Run an entry
LIST         -- List entries
ADD entry    -- Add an entry
EDIT entry   -- Edit an entry
SHOW entry   -- Show an entry
TEST entry   -- Test an entry
DEL entry    -- Delete an entry
DUP old new  -- Duplicate old entry to new one
QUIT         -- Exit the program
What to do>
```

You can specify the path of the vault pickle in the SECURE_PATH global variable.

# Requirements

* Python3
* GNU screen or tmux -- for terminal multiplexing
* openssl -- for encryption
